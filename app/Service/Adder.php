<?php

namespace App\Service;

class Adder
{
    protected $sum = 0;

    public function sum($a, $b)
    {
        return $a + $b;
    }

    public function add($a)
    {
        $this->sum += $a;
    }

    public function getSum()
    {
        return $this->sum;
    }
}