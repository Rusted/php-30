<?php

namespace App\Service;

class Information
{
    protected $messages = [];

    public function addMessage($message)
    {
        $this->messages[]= $message;
    }

    public function getMessages()
    {
        return $this->messages;
    }
}