<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\Car' => 'App\Policies\CarPolicy',
        'App\Document' => 'App\Policies\DocumentPolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('car.update', 'App\Policies\CarPolicy@update');
        
        Gate::resource('document', 'App\Policies\DocumentPolicy', [
            'download',
            'upload',
            'list' 
        ]);
        //
    }
}
