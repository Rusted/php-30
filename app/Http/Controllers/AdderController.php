<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdderController extends Controller
{
    public function __construct()
    {
        $adder = resolve('app.adder');
        $adder->add(5);
        var_dump($adder);
    }

    public function sum()
    {
        $adder = resolve('app.adder');
        var_dump($adder->sum(2,10));

        $adder->add(5);
        var_dump($adder);
        die();
    }
}
