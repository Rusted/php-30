<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\Information;

class InformationController extends Controller
{
    public function __construct(Information $information)
    {
        $information->addMessage('message 1');
    }

    public function information(Information $information)
    {
        $information->addMessage('message 2');

        return view('information', ['messages' => $information->getMessages()]);
    }
}
