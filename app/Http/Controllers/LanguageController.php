<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class LanguageController extends Controller
{
    public function change(Request $request)
    {
        Session::put('lang', $request->input('language'));
        return \Redirect::back()->withSuccess( "Kalba pakeista " );
    }
}
