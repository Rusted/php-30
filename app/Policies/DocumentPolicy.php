<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Document;

class DocumentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function list(User $user)
    {
        return in_array($user->role, [User::ROLE_ADMIN, User::ROLE_NORMAL]);
    }

    public function upload(User $user)
    {
        return in_array($user->role, [User::ROLE_ADMIN]);
    }

    public function download(User $user, Document $document)
    {
        return in_array($user->role, [User::ROLE_ADMIN, User::ROLE_NORMAL]);
    }
}
