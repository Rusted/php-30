<?php

use Illuminate\Database\Seeder;

class CarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car')->insert([
            'reg_number' => str_random(5),
            'brand' => str_random(5),
            'model' => str_random(5),
            'owner_id' => 1,
            'user_id' => 1,
            'id' => 1
        ]);

        DB::table('car')->insert([
            'reg_number' => str_random(5),
            'brand' => str_random(5),
            'model' => str_random(5),
            'owner_id' => 2,
            'user_id' => 2,
            'id' => 2
        ]);
    }
}
