<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@test.com',
            'id' => 1,
            'role' => App\User::ROLE_ADMIN,
            'password' => Hash::make('123456')
        ]);
        DB::table('users')->insert([
            'name' => 'normal guy',
            'email' => 'normal@test.com',
            'id' => 2,
            'role' => App\User::ROLE_NORMAL,
            'password' => Hash::make('123456')
        ]);
    }
}
