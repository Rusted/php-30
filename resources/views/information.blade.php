@extends('layouts/app')

@section('content')
    <ul>
    @foreach($messages as $message)
        <li>{{$message}}</li>
    @endforeach
    </ul>
@endsection